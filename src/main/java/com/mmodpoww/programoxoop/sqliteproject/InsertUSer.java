/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmodpoww.programoxoop.sqliteproject;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class InsertUSer {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            Statement stmt = conn.createStatement();
            String sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (1 , 'Danny' , 'Hyunsuk');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (2 , 'June' , 'Jihoon');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (3 , 'Jaden' , 'Yoshi');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (4 , 'David' , 'Junkyu');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (5 , 'Mama' , 'Mashiho');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (6 , 'Kevin' , 'JaeHyuk');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (7 , 'Arthur' , 'Asahi');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (8 , 'Kyle' , 'Yedam');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (9 , 'Sam' , 'Doyoung');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (10 , 'Travis' , 'Haruto');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (11 , 'Justin' , 'Jeongwoo');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (12 , 'John' , 'Junghwan');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (13 , 'Yuta' , '123');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (14 , 'Mino' , '456');";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
